<!DOCTYPE html>
<html>
   <head>
        <meta charset='utf-8'>
        <title>ESTUMUSICA_ROCK</title>

	<link rel="icon" href="favicon.ico" >
        <link rel="stylesheet" href="css/bootstrap.css">
        <link rel="stylesheet" href="css/font-awesome.min.css">
    	<link href="http://vjs.zencdn.net/5.4.6/video-js.css" rel="stylesheet">
  	<script src="http://vjs.zencdn.net/ie8/1.1.1/videojs-ie8.min.js"></script>
        <script src="js/jquery-3.3.1.min.js"></script>
        <script src="js/bootstrap.min.js"></script>
        <script src="https://content.jwplatform.com/libraries/4olVzijN.js"></script>
        
        <style type="text/css">
            .center {
                display: block;
                margin-left: auto;
                margin-right: auto;
                width: 50%;
            }
    
            .button{
                -webkit-appearance:button;
                -moz-appearance:button;
                appearance:button;
                background-color:#00A2E8;
                text-decoration:none;
                color:white;
                font-family:sans-serif;
                
                border-radius: 12px;
                border-collapse: separate;
                border: 2px;
                
                display:block;
                margin-left: auto;
                margin-right: auto;
                width: 150px;
                padding:10 px 5 px;
                
                text-align: center;
            }
            
            .videosMusica > video{
               
                margin-bottom: 0px;
                padding:0;
                
                
                display: block;
                margin-left: auto;
                margin-right: auto;
                width: 50%;
                
                max-height: 720px;
                margin: 0 auto;
            }
            
            .tituloCancion{
                display: block;
                margin-left: auto;
                margin-right: auto;
                width: 50%;
                text-align:center;
            }
            
            .ul{
                list-style-type: none;
                margin: 0;
                padding: 0;
                overflow: hidden;
                background-color: #333;
                color:white;
                
                position:fixed;
                top:0;
                width:100%;
            }
            
            .li{
                float: left;
            }
            
            .li a{
                display:block
                color: white;
                text-align: center;
                padding: 14px 16px;
                text-decoration: none;
                
                border-right: 1px solid #bbb;
            }
            
            .li:last-child{
                border-right: none;
            }
            
            .li a:hover:not(.active){
                background-color: #111;
            }
            
            .active{
                background-color: #4CAF50;
            }
            
            .filaBotones{
                text-align: center;
            }
                      
        </style>
    </head>
<body background="Imagenes/fondo.jpg">

    <div id="LOGO">
        <a href="EsTuMusica_Home.html">
            <img src="Imagenes/logoFinal.png" class="center"/>
        </a>
    </div>
    
    <ul>
            <li><a href="EsTuMusica_Home.html">Home</a></li>
            <li><a href="EsTuMusica_Rock.php">Rock</a></li>  
            <li><a href="EsTuMusica_Pop.php">Pop</a></li>
            <li><a href="EsTuMusica_Electronica.php">Electronica</a></li> 
            <li><a href="EsTuMusica_Clasica.php">Clasica</a></li>
            <li><a href="EsTuMusica_About.html">Nosotros</a></li>
            <li><a href="EsTuMusica_Subida.html">Sube un video</a></li>
            <li><a href="EsTuMusica_TusVideos.php">Tus videos</a></li>
        </ul>     
    <div align="center">	
        <form action="subida.php" method="post" enctype="multipart/form-data">
            <font color="red">
            <input type="file" name="archivo"></input>
            <input type="submit" value="Subir archivo"></input>
        </form>	
    </div>
    
    <select id="ddlViewBy">
        <option value="1">test1</option>
        <option value="2">test2</option>
        <option value="3">test3</option>
    </select>
    
    $opcion=document.getElementById("ddlViewBy");
    $cogido=e.options[e.selectedIndex].text;

    </body>
</html>
