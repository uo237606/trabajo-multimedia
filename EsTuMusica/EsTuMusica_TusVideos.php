<!DOCTYPE html>
<html>
    <head>
        <meta charset='utf-8'>
        <title>ESTUMUSICA_ROCK</title>

	<link rel="icon" href="favicon.ico" >
        <link rel="stylesheet" href="css/bootstrap.css">
        <link rel="stylesheet" href="css/font-awesome.min.css">
    	<link href="http://vjs.zencdn.net/5.4.6/video-js.css" rel="stylesheet">
  	<script src="http://vjs.zencdn.net/ie8/1.1.1/videojs-ie8.min.js"></script>
        <script src="js/jquery-3.3.1.min.js"></script>
        <script src="js/bootstrap.min.js"></script>
        <script src="https://content.jwplatform.com/libraries/4olVzijN.js"></script>
        
        <style type="text/css">
            .center {
                display: block;
                margin-left: auto;
                margin-right: auto;
                width: 50%;
            }
    
            .button{
                -webkit-appearance:button;
                -moz-appearance:button;
                appearance:button;
                background-color:#00A2E8;
                text-decoration:none;
                color:white;
                font-family:sans-serif;
                
                border-radius: 12px;
                border-collapse: separate;
                border: 2px;
                
                display:block;
                margin-left: auto;
                margin-right: auto;
                width: 150px;
                padding:10 px 5 px;
                
                text-align: center;
            }
            
            .videosMusica > video{
               
                margin-bottom: 0px;
                padding:0;
                
                
                display: block;
                margin-left: auto;
                margin-right: auto;
                width: 50%;
                
                max-height: 720px;
                margin: 0 auto;
            }
            
            .tituloCancion{
                display: block;
                margin-left: auto;
                margin-right: auto;
                width: 50%;
                text-align:center;
            }
            
            .ul{
                list-style-type: none;
                margin: 0;
                padding: 0;
                overflow: hidden;
                background-color: #333;
                color:white;
                
                position:fixed;
                top:0;
                width:100%;
            }
            
            .li{
                float: left;
            }
            
            .li a{
                display:block
                color: white;
                text-align: center;
                padding: 14px 16px;
                text-decoration: none;
                
                border-right: 1px solid #bbb;
            }
            
            .li:last-child{
                border-right: none;
            }
            
            .li a:hover:not(.active){
                background-color: #111;
            }
            
            .active{
                background-color: #4CAF50;
            }
            
            .filaBotones{
                text-align: center;
            }
                      
        </style>
    </head>
    
    <body>
        
        <body background="Imagenes/fondo.jpg">
        <div id="LOGO">
            <a href="EsTuMusica_home.html">
                <img src="Imagenes/logoFinal.png" class="center"/>
            </a>
        </div>
            
        <ul>
            <li><a href="EsTuMusica_Home.html">Home</a></li>
            <li><a href="EsTuMusica_Rock.php">Rock</a></li>  
            <li><a href="EsTuMusica_Pop.php">Pop</a></li>
            <li><a href="EsTuMusica_Electronica.php">Electronica</a></li> 
            <li><a href="EsTuMusica_Clasica.php">Clasica</a></li>
            <li><a href="EsTuMusica_About.html">Nosotros</a></li>
            <li><a href="EsTuMusica_Subida.html">Sube un video</a></li>
            <li><a href="EsTuMusica_TusVideos.php">Tus videos</a></li>
        </ul>  

<?php
                $ruta="/usr/local/WowzaStreamingEngine/content";
                $filehandle=opendir($ruta);
                print "VÍDEOS EN STREAMING";
                while($dir = readdir($filehandle)){

                    $string = array_pop(explode('.',$dir));
                   
                    if($dir != "." && $dir != ".." && $string=="mp4"){
			$nombre = array_shift(explode('.',$dir));
			$nombreOri = array_pop(explode('---',$nombre));
		
            ?>

    		<div><h2><?php echo $nombreOri;?></h2></div>
       		<video id="<?php echo $nombre; ?>" class="video-js" controls preload="auto" width="480" height="270" 
         poster="Imagenes/<?php echo $nombre;?>.png" 
         data-setup="{}" align="center">
        <source src="rtmp://<php? localhost ?>:1935/bajodemanda/<?php echo $nombre;?>.mp4" type="rtmp/mp4">
                                
                
  </video>


		
            <?php   

                    }
                    
                    

                }
            
            
            
           $ruta1="Subidas";
                $filehandle1=opendir($ruta1);
            print "VÍDEOS ALOJADOS LOCALMENTE";
                while($dir = readdir($filehandle1)){

                    $string1 = array_pop(explode('.',$dir));
                   
                    if($dir != "." && $dir != ".." && $string1=="mp4"){
			$nombre1 = array_shift(explode('.',$dir));
			$nombreOri1 = array_pop(explode('---',$nombre1));
		
            ?>

    		<div><h2><?php echo $nombre1;?></h2></div>
       		<video id="<?php echo $nombre1; ?>" class="video-js" controls preload="auto" width="480" height="270" 
         poster="Imagenes/<?php echo $nombre1;?>.jpg" 
         data-setup="{}">
        <source src="Subidas/<?php echo $nombre1;?>.mp4" type="video/mp4">
                                
                
  </video>


		
            <?php   

                    }
                    
                    

                } 
            
            
            
            
            
            
            
            
            
                closedir($filehandle);
            closedir($filehandle1);
		
            ?>
            
        </div>  
            <script src="http://vjs.zencdn.net/5.4.6/video.js"></script> 
    </body>
</html>
